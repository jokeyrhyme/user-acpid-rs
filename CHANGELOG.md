# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- example systemd user unit file: [user-acpid.service](./user-acpid.service)

- run `~/.config/user-acpid/button-power-press` when we press the power button
