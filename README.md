# user-acpid-rs [![Status](https://img.shields.io/badge/status-actively--developed-brightgreen)](https://gitlab.com/jokeyrhyme/user-acpid-rs) [![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/jokeyrhyme/user-acpid-rs?branch=main)](https://gitlab.com/jokeyrhyme/user-acpid-rs/-/pipelines?ref=main) [![Crates.io](https://img.shields.io/crates/v/user-acpid)](https://crates.io/crates/user-acpid) [![docs.rs](https://img.shields.io/docsrs/user-acpid)](https://docs.rs/user-acpid)

non-root companion process for linux acpid ( https://sourceforge.net/projects/acpid2/ )

## status

- works!

## why

- we want a way to react to ACPI events and act upon an interactive user session
- this means being able to easily access the current dbus/wayland/pipewire/etc sessions
- [`acpid`](https://sourceforge.net/projects/acpid2/) is great,
  but it executes hooks as root instead of as the current interactive user
- it's quite (intentionally) fiddly for anyone except that user to access user session sockets, buses, etc
- this tool is intended to be executed as a non-root user in the context of their interactive session

## use cases

- in combination with [`wlr-randr`](https://sr.ht/~emersion/wlr-randr/),
  turn on/off a built-in display based on laptop lid status
- turn on/off a built-in USB webcam based on laptop lid status
- in combination with [`pw-cli`](https://gitlab.freedesktop.org/pipewire/pipewire),
  turn on/off a built-in USB microphone based on laptop lid status

## dependencies

- requires your system to be running `acpid` with the default `--socketfile` behaviour

## roadmap

- [x] read events from /var/run/acpid.socket
- [x] identify lid open and lid close ACPI events
- [x] run ~/.config/user-acpid/button-lid-open when opened
- [x] run ~/.config/user-acpid/button-lid-close when closed
- [x] check lid status on startup
- [ ] support alternative paths to `acpid`'s socket file
- [ ] handle socket closure and attempt to re-open after a delay
- [ ] close and re-open socket after receiving SIGUSER1 signal
- [ ] explore direct kernel access (independence from `acpid`)
